package flyg;																				//
import java.sql.ResultSet;																	//H�r skapar anv�ndaren ett nytt flygplan som genom anv�ndarens
public class Flygplan extends HuvudMeny														//inmatning blir kopplat till ett flygbolag. 
{																							//�terigen finns en utsskriftsfunktion som listar f�rst
	String query[] = {"select flygnummer from flygplan where flygnummer = ?", 				//redan befintliga flygplan med tillh�rande flygbolag
			"select flygbolag_namn from flygbolag where flygbolag_namn = ?"};				//och efter inmatningen uppdateringen.
	String error = "Detta flygplan existerar redan. V�lj ett nytt flygnummer!";
	String ExistError = "Detta flygbolag finns inte. V�lj ett nytt flygbolag!";

	public Flygplan() 
	{
		Validering valid = new Validering();
		printFlygplan();
		
		String flygbolag;
		System.out.println("Ange nytt flygplansnummer: ");
		inputText = user_input.next();
		inputText = inputText + user_input.nextLine();
		while(!valid.SQLvalidering(inputText, query[0], error))
		{
		inputText = user_input.next();
		inputText = inputText + user_input.nextLine();
		}
		System.out.println("Ange namn p� flygplanets flygbolag: ");
		flygbolag = user_input.next();
		flygbolag = flygbolag + user_input.nextLine();
		while(!valid.SQLvalideringExist(flygbolag, query[1], ExistError))
		{
		flygbolag = user_input.next();
		flygbolag = flygbolag + user_input.nextLine();
		}
		
		try
		{	
			myStmt.executeUpdate("insert ignore into flygplan (flygnummer, flygbolag) "
					+ "values ('" + inputText + "','" + flygbolag + "')");
			 printFlygplan();
		}
		catch (Exception exc)
		{
			exc.printStackTrace();
		}
		
	}
		public void printFlygplan()
		{
			try
			{
			ResultSet myRs = myStmt.executeQuery("select * from flygplan");
			
			while (myRs.next()){
				System.out.println(myRs.getString("flygnummer")+ ", "+ 
			myRs.getString("flygbolag")+ "\n");
			}
			}
			catch (Exception exc)
			{
				exc.printStackTrace();
			}
}
}