package flyg;																			//I denna klass fylls databasen HG med sina tabeller
import java.sql.*;																		//och deras tillhörand kolumner, samt förser alla
public class Skapadb extends HuvudMeny													//tabeller utom kund och bokning med ett antal
{																						//data (ville inte lämna det helt tomt); allt inom ett try-catch-block.
																						//Tabellerna flygplan, flyglinje och bokning innehåller foreign keys.
	public Skapadb()
	{
		try
		{
			myConn = DriverManager.getConnection(url, user, pass);
			myStmt = myConn.createStatement();
			myStmt.executeUpdate("create database if not exists HG");
			database = "HG";
			myStmt.executeUpdate("use "+database);
			myStmt.executeUpdate("create table if not exists kund "
					+ "(id int(5)not null primary key auto_increment, "
					+ "persnr varchar(13)not null unique, f_namn varchar(10)not null, "
					+ "e_namn varchar(20)not null, email varchar(30)not null, tele_nr varchar(20)not null);");
			myStmt.executeUpdate("create table if not exists flygplats "
					+ "(id int(3)not null primary key auto_increment, "
					+ "flygplats_namn varchar(3)not null unique, ort varchar(20)not null, land varchar(20)null);");
			myStmt.executeUpdate("create table if not exists flygbolag "
					+ "(id int(3)not null primary key auto_increment, "
					+ "flygbolag_namn varchar(30)not null unique);");
			myStmt.executeUpdate("create table if not exists flygplan "
					+ "(flygnummer varchar(8)not null primary key, "
					+ "flygbolag varchar(30)not null, "
					+ "constraint flygbolag foreign key(flygbolag) references flygbolag(flygbolag_namn));");
			myStmt.executeUpdate("create table if not exists flyglinje "
					+ "(id int(3)not null primary key auto_increment, "
					+ "flygbolag varchar(30)not null, avg_flygplats varchar(3)not null, ank_flygplats varchar(3)not null,"
					+ "flygplan varchar(8)not null, datum date not null unique,"
					+ "constraint flygbolag2 foreign key(flygbolag) references flygbolag(flygbolag_namn),"
					+ "constraint avg_flygplats foreign key(avg_flygplats) references flygplats(flygplats_namn),"
					+ "constraint ank_flygplats foreign key(ank_flygplats) references flygplats(flygplats_namn),"
					+ "constraint flygplan foreign key(flygplan) references flygplan(flygnummer));");
			myStmt.executeUpdate("create table if not exists bokning "
					+ "(boknings_id int(11)not null primary key auto_increment, "
					+ "kund_id int(5)not null, flyglinje_id int(3)not null,"
					+ "constraint kund_id foreign key(kund_id) references kund(id),"
					+ "constraint flyglinje_id foreign key(flyglinje_id) references flyglinje(id));");
			
			myStmt.executeUpdate("insert ignore into flygplats "
					+ "(flygplats_namn, ort, land) values ('" + "ARN" + "','" + "Stockholm" + "','" + "Sverige" + "')");
			myStmt.executeUpdate("insert ignore into flygplats "
					+ "(flygplats_namn, ort, land) values ('" + "MMX" + "','" + "Malmö" + "','" + "Sverige" + "')");
			myStmt.executeUpdate("insert ignore into flygplats "
					+ "(flygplats_namn, ort, land) values ('" + "GOT" + "','" + "Göteborg" + "','" + "Sverige" + "')");
			myStmt.executeUpdate("insert ignore into flygplats "
					+ "(flygplats_namn, ort, land) values ('" + "LHR" + "','" + "London" + "','" + "UK" + "')");
			myStmt.executeUpdate("insert ignore into flygplats "
					+ "(flygplats_namn, ort, land) values ('" + "OSL" + "','" + "Oslo" + "','" + "Norge" + "')");
			myStmt.executeUpdate("insert ignore into flygplats "
					+ "(flygplats_namn, ort, land) values ('" + "CPH" + "','" + "Köpenhamn" + "','" + "Danmark" + "')");
			
			myStmt.executeUpdate("insert ignore into flygbolag (flygbolag_namn) values ('Hemflyget')");
			myStmt.executeUpdate("insert ignore into flygbolag (flygbolag_namn) values ('Flygdirekt')");
			myStmt.executeUpdate("insert ignore into flygbolag (flygbolag_namn) values ('Scandiflight')");
			myStmt.executeUpdate("insert ignore into flygbolag (flygbolag_namn) values ('Nextflight')");
			myStmt.executeUpdate("insert ignore into flygbolag (flygbolag_namn) values ('YourDestination')");
			
			myStmt.executeUpdate("insert ignore into flygplan (flygnummer, flygbolag) "	//Stockholm-Malmö
					+ "values ('" + "SK117" + "','" + "Scandiflight" + "')");
			myStmt.executeUpdate("insert ignore into flygplan (flygnummer, flygbolag) "	//Stockholm-Göteborg
					+ "values ('" + "SK161" + "','" + "Scandiflight" + "')");
			myStmt.executeUpdate("insert ignore into flygplan (flygnummer, flygbolag) "	//Stockholm-London
					+ "values ('" + "SK843" + "','" + "Nextflight" + "')");
			myStmt.executeUpdate("insert ignore into flygplan (flygnummer, flygbolag) "	//Stockholm-Köpenhamn
					+ "values ('" + "SK1419" + "','" + "Hemflyget" + "')");
			myStmt.executeUpdate("insert ignore into flygplan (flygnummer, flygbolag) "	//Malmö-Göteborg
					+ "values ('" + "SK707" + "','" + "Flygdirekt" + "')");
			myStmt.executeUpdate("insert ignore into flygplan (flygnummer, flygbolag) "	//Malmö-Norge
					+ "values ('" + "SK611" + "','" + "YourDestination" + "')");
			
			
					myStmt.executeUpdate("insert ignore into flyglinje (flygbolag, avg_flygplats, ank_flygplats,"
					+ "flygplan, datum) values ('Scandiflight', 'ARN', 'MMX', 'SK117', '2015-01-13')");
					myStmt.executeUpdate("insert ignore into flyglinje (flygbolag, avg_flygplats, ank_flygplats,"
					+ "flygplan, datum) values ('Scandiflight', 'ARN', 'GOT', 'SK161', '2015-01-14')");
					myStmt.executeUpdate("insert ignore into flyglinje (flygbolag, avg_flygplats, ank_flygplats,"
					+ "flygplan, datum) values ('Nextflight', 'ARN', 'LHR', 'SK843', '2015-01-15')");
					myStmt.executeUpdate("insert ignore into flyglinje (flygbolag, avg_flygplats, ank_flygplats,"
					+ "flygplan, datum) values ('Hemflyget', 'ARN', 'CPH', 'SK1419', '2015-01-16')");
					myStmt.executeUpdate("insert ignore into flyglinje (flygbolag, avg_flygplats, ank_flygplats,"
					+ "flygplan, datum) values ('Flygdirekt', 'MMX', 'GOT', 'SK1419', '2015-01-17')");
					myStmt.executeUpdate("insert ignore into flyglinje (flygbolag, avg_flygplats, ank_flygplats,"
					+ "flygplan, datum) values ('YourDestination', 'MMX', 'OSL', 'SK611', '2015-01-18')");
					
	
			}
			catch (Exception exc)
			{
				exc.printStackTrace();
			}
	}
}
