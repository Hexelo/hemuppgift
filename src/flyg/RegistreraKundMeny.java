package flyg;																			//I denna klass som flera andra klasser g�r en instans av
																						//registreras en ny kund in i databasen. Persnr och email valideras.
public class RegistreraKundMeny extends HuvudMeny
{
	private KundDb kundDb = new KundDb();
	private Kund kund;
	
	public RegistreraKundMeny()
	{
		String patternPersnr = "[\\d]{6}-[\\d]{4}";
		String patternEmail = "^[a-zA-Z0-9\\.]+@[a-zA-Z0-9\\.]+\\.[a-zA-Z]+$";
		String RegExError = "Felaktig inmatning. F�rs�k igen!";
		String personnr;
		String f_namn;
		String e_namn;
		String email;
		String telefonnr;

		System.out.println("Registrera ny kund\nAnge personnummer (NNNNNN-xxxx): ");
		personnr = user_input.next();
		
		kund = kundDb.findByPersonnummer(personnr);											//H�r anropas objektet kund och om persnr inte f�r en tr�ff i
		if (kund == null)																	//databasen fors�tter registreringen av ny kund genom IF-satsen.
		{

			Validering valid = new Validering();
			while(!valid.regexValid(personnr, patternPersnr, RegExError))
			{
				System.out.println("Ange personnummer (NNNNNN-xxxx): ");
				personnr = user_input.next();
			}
			
			System.out.println("Ange f�rnamn: ");
			f_namn = user_input.next();
			System.out.println("Ange efternamn: ");
			e_namn = user_input.next();
			System.out.println("Ange emailadress: ");
			email = user_input.next();
				while(!valid.regexValid(email, patternEmail, RegExError))
				{
					System.out.println("Ange emailadress: ");
					email = user_input.next();
				}
			
			System.out.println("Ange telefonnummer: ");
			telefonnr = user_input.next();
			
			kund = kundDb.createKund(personnr, f_namn, e_namn, email, telefonnr);
		}
	}

	public Kund getKund() {
		return kund;
	}
}
