package flyg;																		//Hit dirigeras den anv�ndare som angett sig sj�lv som resen�r.
																					//Kunden har 3 alternativ f�r att g�ra en s�kning och f� ut
public class Bokning extends HuvudMeny												//id f�r den flyglinje hen vill boka. N�r en resa �r vald m�ste
{																					//kunden ange personnr f�r att d�refter antingen forts�tta att registrera
	public Bokning()																//sig som ny kund eller g� direkt vidare med de sparade uppgifter ett
	{																				//redan befintligt persnr kopplas till och biljetten skrivs ut.
		Skapadb dbCreate = new Skapadb();
		String query = null;
		String kund_id;
		String flyglinje_id;
		String[] column = {"id","flygbolag","avg_flygplats","ank_flygplats","flygplan","datum"};
		
		
		System.out.println("\nBokningsmeny\n\n1.\tS�k p� avg�ngsflygplats\n2.\tS�k p� ankomstflygplats\n3.\tS�k p� datum");
		System.out.println("V�lj genom att skriva in respektive siffra f�r ditt val: ");
		inputTal = user_input.nextInt();
	
		try
		{

		if(inputTal == 1)
		{
			System.out.println("Ange namn p� avg�ngsflygplats (3 stora bokst�ver) f�r att lista alla flygrutter: ");
			inputText = user_input.next();
			query  = "select * from flyglinje where avg_flygplats ='"+inputText+"'";
			printSearch(query, inputText, column);
		}
		
		if(inputTal == 2)
		{
			System.out.println("Ange namn p� ankomstsflygplats (3 stora bokst�ver) f�r att lista alla flygrutter: ");
			inputText = user_input.next();
			query = "select * from flyglinje where ank_flygplats ='"+inputText+"'";
			printSearch(query, inputText, column);
		}
		
		if(inputTal == 3)
		{
			System.out.println("Ange datum f�r att lista alla flygrutter: ");
			inputText = user_input.next();
			query = "select * from flyglinje where datum ='"+inputText+"'";
			printSearch(query, inputText, column);
		}
			
		}
		catch (Exception exc)
		{
			exc.printStackTrace();
		}
		}
	
	public void printSearch(String query, String arg, String[] column)										//Denna metod inneh�ller 3 argument; query f�r de select-satser
	{																										//som k�rs i IF-satserna, arg f�r inmatningarna och column
																											//f�r de kolumner som ska listas fr�n tabellen flyglinje
		try
		{
			
		System.out.println("ID	Flygbolag		Avg�ng	Ankomst		Flygnummer	Datum\n"
				+ "-----------------------------------------------------------------------------------");
		
		myRs = myStmt.executeQuery(query);
		
		while (myRs.next()){
			System.out.println(myRs.getString(column[0]) + "	" + 
					myRs.getString(column[1])+ "		"+
					myRs.getString(column[2])+ "	"+
					myRs.getString(column[3])+ "		"+
					myRs.getString(column[4])+ "		"+
					myRs.getString(column[5])+ "\n\n");
		}
			System.out.println("-----------------------------------------------------------------------------------"
					+ "\nAnge id f�r den flygrutt du vill boka: ");
			int flygruttId = user_input.nextInt();																//H�r anges id f�r den flyglinje som ska f�ras in i bokningen.
			
			System.out.println("1. \tBekr�fta bokning\n2. \tExit");
			int bokning = user_input.nextInt();
			
			if(bokning == 1)
			{
				RegistreraKundMeny registreraKundMeny = new RegistreraKundMeny();									//H�r skapas instansen av "RegistreraKundMeny" och v�rdet kundId
				int kundId = registreraKundMeny.getKund().getKundId();												//h�mtas fr�n objektet Kund.
				
				try
				{
					myStmt.executeUpdate("insert ignore into bokning (kund_id, flyglinje_id) values ("+kundId+","+flygruttId+")");
					System.out.println("BokningsID	KundID	F�rnamn		Efternamn	Flygbolag	Avg�ng	Ankomst		Flygnummer	Datum\n"
							+ "-----------------------------------------------------------------------------------");
					
					myRs = myStmt.executeQuery("select bokning.boknings_id, bokning.kund_id,"							//Denna select-sats anv�nds f�r att kunna skriva ut en "biljett"
							+ "kund.f_namn, kund.e_namn, flyglinje.flygbolag, flyglinje.avg_flygplats,"					//med data fr�n 3 tabeller med foreign keys.
							+ "flyglinje.ank_flygplats, flyglinje.flygplan, flyglinje.datum from bokning,"
							+ "kund, flyglinje where bokning.kund_id = "+kundId+" and bokning.kund_id = kund.id"
									+ " and bokning.flyglinje_id = flyglinje.id group by bokning.boknings_id");
					
					while (myRs.next())
					{
						System.out.println(myRs.getString("boknings_id") + "		" + 
								myRs.getString("kund_id")+ "		"+
								myRs.getString("f_namn")+ "	"+
								myRs.getString("e_namn")+ "	"+
								myRs.getString("flygbolag")+ "	"+
								myRs.getString("avg_flygplats")+ "		"+
								myRs.getString("ank_flygplats")+ "		"+
								myRs.getString("flygplan")+ "		"+
								myRs.getString("datum")+ "\n\n");
					}
						System.out.println("-----------------------------------------------------------------------------------");
				}
				catch (Exception exc)
				{
					exc.printStackTrace();
				}
			}
			if(bokning == 2)
			{
				System.out.println("V�lkommen �ter!");
			}
		}
		catch (Exception exc)
		{
			exc.printStackTrace();
		}

	}

}
