package flyg;																									//H�r skapar anv�ndaren ett nytt flygbolag.
public class Flygbolag extends HuvudMeny																		//�ven h�r finns en utskriftsfunktion i metoderna
{																												//"printFlygbolag" och "printFlygbolagrutter"
	String patternFlygbolag = "[A-Za-z0-9]{6,30}";																//Instansen "valid" anv�nds f�r att validera namnet p� flygbolaget.
	String query = "select flygbolag_namn from flygbolag where flygbolag_namn = ?";
	String RegExError = "Du har angivit ett felaktigt namn!(6-30 tecken)";
	String SQLerror = "Detta flygbolag existerar redan. V�lj ett nytt!";
	
	public Flygbolag()
	{
		printFlygbolag();
		
		System.out.println("Ange namn p� nytt flygbolag (6-30 tecken): ");
		inputText = user_input.next();
		inputText = inputText + user_input.nextLine();
		Validering valid = new Validering();
		
		while(!valid.regexValidering(inputText, patternFlygbolag, query, RegExError, SQLerror))
		{
			inputText = user_input.next();
			inputText = inputText + user_input.nextLine();
			
		}
		try
		{
			
			myStmt.executeUpdate("insert ignore into flygbolag (flygbolag_namn) values ('" + inputText + "')");
			printFlygbolag();
		}
		catch (Exception exc)
		{
			exc.printStackTrace();
		}
		
	}
	public void printFlygbolag()
	{
		try
		{
		myRs = myStmt.executeQuery("select * from flygbolag order by id asc");
		
		while (myRs.next())
		{
			System.out.println(myRs.getString("id") + "		" + 
			myRs.getString("flygbolag_namn") + "\n");
		}	
		}
		catch (Exception exc)
		{
			exc.printStackTrace();
		}
			System.out.println("Vill du lista alla flygrutter hos ett visst flygbolag? Ja/Nej");
			String val = user_input.next();
				if (val.equals("Ja") || val.equals("ja"))
					printFlygbolagRutter();
	}
	public void printFlygbolagRutter()
	{
		String flygbolag;
		System.out.println("Ange aktuellt flygbolags namn: ");
		flygbolag = user_input.next();
		flygbolag = flygbolag + user_input.nextLine();
		
		try
		{
			myRs = myStmt.executeQuery("select * from flyglinje where flygbolag = '" + flygbolag + "'");
			
			while (myRs.next())
			{
				System.out.println(myRs.getString("id") + "	" + myRs.getString("flygbolag") + "	" + 
						myRs.getString("avg_flygplats") + "	" + myRs.getString("ank_flygplats") + "	" + 
						myRs.getString("flygplan") + "	" + myRs.getString("datum") + "\n");
			}
		}
		catch (Exception exc)
		{
			exc.printStackTrace();
		}
	}
	}

