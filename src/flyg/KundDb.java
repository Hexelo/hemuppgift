package flyg;																						//I denna klass j�mf�rs persnr fr�n inmatningen med databasen
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
																									//och om den finner persnr:et ta med sig v�rden som select-satserna
public class KundDb extends DbConnection															//pekar p� vidare i bokningen.
{

	public Kund findByPersonnummer(String personnummer)
	{
		try
		{
			ResultSet rs = myStmt.executeQuery("select * from kund where persnr = '" + personnummer + "'");
		
			if (rs.next())
			{
				return new Kund(rs.getInt("id"), rs.getString("persnr"), rs.getString("f_namn"),
						rs.getString("e_namn"), rs.getString("email"), rs.getString("tele_nr"));
			}
		} catch (SQLException e) 
		{
			e.printStackTrace();
		}

		return null;
	}
	
	public Kund createKund(String personnr,
		String f_namn,
		String e_namn,
		String email,
		String telefonnr)
	{
		
		try
		{
			myStmt.executeUpdate("insert into kund (persnr, f_namn, e_namn, email, tele_nr) values "
					+ "('" + personnr + "','" + f_namn + "','" + e_namn + "','" + email + "','" + telefonnr + "')",
				Statement.RETURN_GENERATED_KEYS);
			
			ResultSet generatedKeysRs = myStmt.getGeneratedKeys();
			generatedKeysRs.next();
			int kundId = generatedKeysRs.getInt(1);
			
			return new Kund(kundId, personnr, f_namn, e_namn, email, telefonnr);
		} catch (Exception exc)
		{
			exc.printStackTrace();
		}
		
		return null;
	}
	
}
