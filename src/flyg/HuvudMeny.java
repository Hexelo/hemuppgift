package flyg;																//I denna klass (som extendar DbConnection) finns min main-metod. 
																			//I klassen h�mtas metoden Scanner in med namnet user_input
import java.util.Scanner;													//och g�rs till statisk f�r att kunna anv�ndas direkt av subklasserna.
																			//H�r f�r anv�ndaren se den f�rsta menyn och dirigeras om utifr�n
public class HuvudMeny extends DbConnection									//sin inmatning genom tv� IF-satser till antingen Administration-klassen 
{																			//eller kund-/resen�rsdelen med b�rjan i klassen Bokning
	static Scanner user_input = new Scanner(System.in);
	
	public static void main(String[] args)
	{
	
	System.out.println("Huvudmeny\n\n1. \tResen�r\n2. \tAdministrat�r");
	inputTal = user_input.nextInt();
	
		if(inputTal == 1)
		{
			Bokning bokning = new Bokning();
			inputTal = 0;
		}
		if(inputTal == 2)
		{
			Administration admin = new Administration();
			inputTal = 0;
		}
	}

}
