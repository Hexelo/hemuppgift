package flyg;																						//Denna klass innehåller det objekt som anropas
																									//i flera andra klasser.
public class Kund
{
	private int kundId;																									//statements
	private String personnr;
	private String f_namn;
	private String e_namn;
	private String email;
	private String telefonnr;

	public Kund(int kundId, String personnr, String f_namn, String e_namn, String email,								//Kund(argument)
			String telefonnr)
	{
		this.kundId = kundId;																							//body
		this.personnr = personnr;
		this.f_namn = f_namn;
		this.e_namn = e_namn;
		this.email = email;
		this.telefonnr = telefonnr;
	}
	
	public int getKundId()
	{
		return kundId;
	}

	public String getPersonnr()
	{
		return personnr;
	}
}
