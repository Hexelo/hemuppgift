package flyg;																	//H�r b�rjar admin-delen med sin huvudmeny och undermeny.
import java.sql.SQLException;													//Inuti konstruktorn skapas en instans av klassen Skapadb,
																				//och k�r igenom den klassen.
public class Administration extends HuvudMeny									//Varje IF-sats anropar varsin klass f�r att ge �tkomst till eller
	{																			//l�gga till data i databasen.
	
	public Administration()
	{
		Skapadb dbCreate = new Skapadb();
		String query = "select * from bokning";
		String column[] = {"boknings_id", "kund_id", "flyglinje_id"};
		
		while (true)
			{
			System.out.println("\nMeny\n\n1.\tSkapa flygplats\n2.\tSkapa flygbolag\n3.\tSkapa flygplan\n4.\tSkapa flygplinje\n5.\tLista bokningar\n");
		System.out.println("V�lj genom att skriva in respektive siffra f�r ditt val: ");
		
		
		inputTal = user_input.nextInt();
		
		if(inputTal == 1)
		{
			Flygplats skapaFlygplats = new Flygplats();							//skapar en instans av klassen Flygplats
		}
		
		if(inputTal == 2)
		{
			Flygbolag skapaFlygbolag = new Flygbolag();	
		}
		
		if(inputTal == 3)
		{
			Flygplan skapaFlygplan = new Flygplan();
		}
		
		if(inputTal == 4)
		{
			
			Flyglinje skapaFlyglinje = new Flyglinje();
		}
		if (inputTal == 5)
		{
			
			printBokning(query, column);
		}
			System.out.println("1. \tG�r nytt val\n2. \tExit");
			inputTal = user_input.nextInt();
			if(inputTal == 2)
			{
				System.out.println("V�lkommen �ter!");
				break;
			}
		
			
	}

}

	private void printBokning(String query, String column[])													//I denna metod finns en utskriftsfunktion som anropas
	{																											//inuti den 5:e IF-satsen fr�n konstruktorn. Metoden
																												//returnerar tv� argument som deklarerats i b�rjan
		try																										//av konstruktorn.
		{
			myRs = myStmt.executeQuery(query);
			System.out.println("BokningsID		KundID			FlyglinjeID\n"
					+ "-------------------------------------------------------------------------------");
		while (myRs.next())
		{
			System.out.println(myRs.getString(column[0]) + "			" + 
					myRs.getString(column[1])+ "			"+
					myRs.getString(column[2])+  	"\n\n");
			System.out.println("-------------------------------------------------------------------------------");
			
		}
		} catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
	}
}