package flyg;																					//H�r skapas kopplingen mellan flygplats, flygbolag och flygplan
public class Flyglinje extends HuvudMeny														//och ett avresedatum matas in till kopplingen. Alla delar valideras
{																								//och j�mf�rs med databasen enligt sina respektive krav.
	String query[] = {"select flygbolag_namn from flygbolag where flygbolag_namn = ?"			//I utskriftsfunktionen listas databasens uppdaterade inneh�ll
			, "select flygplats_namn from flygplats where flygplats_namn = ?"					//i tabellen Flyglinje.
			, "select flygplats_namn from flygplats where flygplats_namn = ?"
			, "select flygnummer from flygplan where flygnummer = ?"
			, "select datum from flyglinje where datum = ?"};
	
	String[] error = {"Flygbolaget existerar inte. V�lj ett befintligt!","Flygplatsen existerar inte. V�lj ett befintligt!"
			,"Flygplatsen existerar inte. V�lj ett befintligt!","Flygplanet existerar inte. V�lj ett befintligt!"
			,"Datumet �r upptaget, endast 1 flyg per datum!"};
	
	public Flyglinje()
	{
		printFlyglinje();
		
		String flygbolag;
		String avg_flygplats;
		String ank_flygplats;
		String flygnummer;
		String datum;
		Validering valid = new Validering();
		
		System.out.println("Skapa ny flyglinje. Ange flygbolagnamn: ");
		flygbolag = user_input.next();
		flygbolag = flygbolag + user_input.nextLine();											//Dena linje l�ggs till f�r att till�ta namn med mellanslag
		while(!valid.SQLvalideringExist(flygbolag, query[0], error[0]))
		{
			flygbolag = user_input.next();
		}
		System.out.println("Ange avg�ngsflygplats: ");
		avg_flygplats = user_input.next();
		while(!valid.SQLvalideringExist(avg_flygplats, query[1], error[1]))
		{
			avg_flygplats = user_input.next();
		}
		System.out.println("Ange ankomstflygplats: ");
		ank_flygplats = user_input.next();
		while(!valid.SQLvalideringExist(ank_flygplats, query[2], error[2]))
		{
			ank_flygplats = user_input.next();
		}
		System.out.println("Ange flygnummer: ");
		flygnummer = user_input.next();
		flygnummer = flygnummer + user_input.nextLine();
		while(!valid.SQLvalideringExist(flygnummer, query[3], error[3]))
		{
			flygnummer = user_input.next();
		}
		System.out.println("Ange datum: ");
		datum = user_input.next();
		while(!valid.SQLvalidering(datum, query[4], error[4]))
		{
			datum = user_input.next();
		}
		try
		{	
			myStmt.executeUpdate("insert ignore into flyglinje (flygbolag, avg_flygplats, ank_flygplats,"
					+ "flygplan, datum) values ('" + flygbolag + "','" + avg_flygplats + "','" + ank_flygplats + "','" + flygnummer + "','" + datum + "')");
			printFlyglinje();
			
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
		
	}
	public void printFlyglinje()
	{
		try
		{
		myRs = myStmt.executeQuery("select * from flyglinje");
		
		while (myRs.next()){
			System.out.println(myRs.getString("id") + ", " + 
		myRs.getString("flygbolag")+ ", "+
		myRs.getString("avg_flygplats")+ ", "+
		myRs.getString("ank_flygplats")+ ", "+
		myRs.getString("flygplan")+ ", "+
		myRs.getString("datum")+ "\n");
		}
		}
		catch (Exception exc)
		{
			exc.printStackTrace();
		}

	}
}