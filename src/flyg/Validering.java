package flyg;																			//I denna klass finns 4 olika metoder; 2 f�r regex-j�mf�relser,
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.regex.*;
public class Validering 																//2 bara f�r att leta efter matchningar i databasen.
{
	protected static String database = "";
	protected static Connection myConn = null;
	protected ResultSet myRs = null;
	protected static Statement myStmt = null;
	
	public boolean regexValidering(String Arg, String p, String q, String regExError, String SQLerror)		//H�r j�mf�rs inmatningen i klasserna Flygplats och Flygbolag
	{																										//f�r att kontrollera att inmatningen matchar kraven.
	Pattern Compile = Pattern.compile(p);
	Matcher match = Compile.matcher(Arg);
	
	if (!match.find())
	{
		System.out.println(regExError);
	}
	else
	{
		boolean Kontroll = SQLvalidering(Arg, q, SQLerror);
		if(Kontroll)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	return false;
	}
	public boolean SQLvalidering(String arg, String query, String error)										//I denna metod kontrolleras det att inmatningens v�rde �r unikt,
	{																											//annars visas ett felmeddelande (specifierat i respektive klass
		try																										//som str�ngen "SQLerror") f�r att motverka dubbelinmatning.
		{
		myConn = DriverManager.getConnection(DbConnection.url, DbConnection.user, DbConnection.pass);
		myStmt = myConn.createStatement();
		myStmt.executeUpdate("create database if not exists HG");
		database = "HG";
		myStmt.executeUpdate("use "+database);
		
		final PreparedStatement ps = myConn.prepareStatement(query);
		ps.setString(1, arg);
		final ResultSet rs = ps.executeQuery();
		
		if (rs.next())
		{
			System.out.println(error);
			return false;
		}
		else
		{
			return true;
		}
		}
		catch (Exception exc)
		{
			exc.printStackTrace();
		}
		return false;
	}
	public boolean SQLvalideringExist(String arg, String query, String ExistError)								//H�r g�rs samma validering f�r flygplan med skillnaden
	{																											//att den ska felmeddela om flygbolaget som ska koplas till planet
			try																									//inte finns i databasen.
			{
			myConn = DriverManager.getConnection(DbConnection.url, DbConnection.user, DbConnection.pass);
			myStmt = myConn.createStatement();
			myStmt.executeUpdate("create database if not exists HG");
			database = "HG";
			myStmt.executeUpdate("use "+database);
			
			final PreparedStatement ps = myConn.prepareStatement(query);
			ps.setString(1, arg);
			final ResultSet rs = ps.executeQuery();
			
			if (rs.next())
			{
				return true;
			}
			else
			{
				System.out.println(ExistError);
				return false;
			}
			}
			catch (Exception exc)
			{
				exc.printStackTrace();
			}
			return false;
	}
	
	 	public boolean regexValid(String Arg, String p, String regExError)									//Denna metod anv�nds f�r att validera regex-kraven
	 	{																									//f�r persnr och email i RegistreraKundMeny.
			Pattern Compile = Pattern.compile(p);
			Matcher match = Compile.matcher(Arg);
		
			if (!match.find())
			{
			System.out.println(regExError);
			return false;
			}
			else
			{
					return true;
			}

		}
}
