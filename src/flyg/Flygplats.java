package flyg;																						//Denna klass inneh�ller anrop till 2 valideringsfunktioner,
public class Flygplats extends HuvudMeny															//errormeddelanden, instansen "valid" och en utskriftsfunktion.
{																									//H�r f�r anv�ndaren ange namn p� den flygplats som ska skapas
	String patternFlygplats = "[A-Z]{3}";															//
	String query = "select flygplats_namn from flygplats where flygplats_namn = ?";
	String regExError = "Du har angivit ett felaktigt namn!(3 stora bokst�ver)";
	String SQLerror = "Denna flygplats existerar redan. V�lj ett nytt namn!";
	
	public Flygplats() 
	{
		printFlygplats();
	
		String ort;
		String land;
		System.out.println("Ange namn p� ny flygplats (3 stora bokst�ver): ");
		inputText = user_input.next();
		Validering valid = new Validering();
		
		while(!valid.regexValidering(inputText, patternFlygplats, query, regExError, SQLerror))		//Genom den skapade instansen av klassen Validering som inneh�ller
		{																							//fyra argument g�rs h�r en validering av huruvida inmatningen
			inputText = user_input.next();															//st�mmer �verens med regex-kraven. Genom str�ngen "query" j�mf�rs
		}																							//ocks� inmatningen med inneh�llet i databasen.
			System.out.println("Ange flygplatsens ort: ");
			ort = user_input.next();

			System.out.println("Ange land: ");
			land = user_input.next();
		try
	{
			myStmt.executeUpdate("insert ignore into flygplats (flygplats_namn, ort, land) values ('" + inputText + "','" + ort + "','" + land + "')");
			printFlygplats();
		}
		catch (Exception exc)
		{
			exc.printStackTrace();
		}
			
	}
	public void printFlygplats()
	{
		try
		{
		myRs = myStmt.executeQuery("select * from flygplats");
		
		while (myRs.next()){
			System.out.println(myRs.getString("id") + ", " + 
		myRs.getString("flygplats_namn")+ ", "+
		myRs.getString("ort")+ ", "+
		myRs.getString("land")+ "\n");
		}
		}
		catch (Exception exc)
		{
			exc.printStackTrace();
		}

	}
}


